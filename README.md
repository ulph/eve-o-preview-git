NOTE: this project had a weird existance. Originally, a Mercurial repo @ bitbucket. Later, a git repo @ github. Moved back to GitHub (this repo), but now seemingly cloned over @ github again...

-> https://github.com/Phrynohyas/eve-o-preview

I no longer update the project since I stopped playing the game years ago.

# Eve-O preview #

Created to aid playing with multiple clients in CCP game EVE-Online.

It's essentially a nice task switcher, it does not relay any keyboard/mouse events and suchlike.

The code is a bit of a mess, my (ulph / StinkRay) apologies, but do make a pull request etc if you want to contribute!

**Created by**

StinkRay

**Maintained by**

* StinkRay

* Makari Aeron

**With contributions from**

* CCP FoxFour

**Current thread**

https://forums.eveonline.com/default.aspx?g=posts&t=484927&find=unread

**Original thread**

https://forums.eveonline.com/default.aspx?g=posts&t=246157